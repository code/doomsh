/**
 * Copyright (C) 2016, 2018, 2021 Tom Ryder <tom@sanctum.geek.nz>
 *
 * This file is part of doomsh.
 *
 * doomsh is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * doomsh is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * doomsh.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * mem.c -- Exhaust your memory
 *
 * This won't do what you expect on Linux if you don't have a ulimit set.
 * Uncomment the memset(3) call at the end of the loop to fill the pointer if
 * you really want to actually use the memory. Do you trust your OOM though?
 */
int main(void){
	void *ptr = NULL;
	unsigned long mb = 5;
	for (;;mb += 5) {
		fprintf(stderr, "%lu MB\n", mb);
		if ((ptr = realloc(ptr, mb * 1000000)) == NULL) {
			raise(SIGABRT);
		}
		/* memset(ptr, 0, mb * 1000000); */
	}
}
