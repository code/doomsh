/**
 * Copyright (C) 2016, 2018, 2021 Tom Ryder <tom@sanctum.geek.nz>
 *
 * This file is part of doomsh.
 *
 * doomsh is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * doomsh is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * doomsh.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

void rec(int *ip);

/* stack.c -- Cause a stack overflow */
int main(void) {
	int i = 0;
	rec(&i);
}
void rec(int *ip) {
	char i[256];
	printf("%u\n", (*ip)++);
	rec(ip);
}
