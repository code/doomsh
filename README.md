doomsh
======

Use `ulimit` to severely restrict your Bash shell's resources, and see how long
you can last before segfaulting or giving up.

The author highly recommends you create a dummy user to run this, so that you
don't end up munging important files in your `HOME`.

    $ sudo useradd -m doomguy
    $ sudo -i -u doomguy
    $ git clone https://dev.sanctum.geek.nz/code/doomsh.git
    $ cd doomsh
    $ ./doomsh

This software is not really important or useful in any way, but it's kind of
fun to see what breaks when resources are really tight, especially when it
exposes problems in programs like failing to check for `NULL` returns from
`malloc(3)`.

On the author's machine, at level 5, even `ls(1)` doesn't work, let alone Vim.

If you're really crazy, you'll try and launch X11 processes from it, and watch
them twitch as they die.

License
-------

Copyright (C) 2016, 2018, 2021 Tom Ryder <tom@sanctum.geek.nz>

Distributed under GNU General Public License version 3 or any later version.
Please see `COPYING`.
