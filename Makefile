.PHONY: all install clean
.SUFFIXES:
.SUFFIXES: .bash
ALL = doomsh
BASH = /bin/bash
PREFIX = /usr/local
all:  $(ALL)
.bash:
	$(BASH) -c :
	awk -v interpreter=$(BASH) 'NR == 1 { $$1 ="#!" interpreter } 1' $< > $@
	chmod +x ./$@
install: checkem
	mkdir -p -- $(PREFIX)/bin
	cp -- doomsh $(PREFIX)/bin
clean:
	rm -f -- $(ALL)
